﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calc.BAL
{
    public class Calculator
    {
        private double _num1;
        private double _num2;
        private string _operator;
        private static readonly string[] _validOperators = new string[] { "+", "-", "*", "/" };

        public double Num1
        {
            get { return _num1; }
            set { _num1 = value; }
        }

        public double Num2
        {
            get { return _num2; }
            set { _num2 = value; }
        }

        public string Operator
        {
            get { return _operator; }
            set { _operator = value; }
        }

        public static string[] ValidOperators
        {
            get { return _validOperators; }
        }

        private double Add()
        {
            return _num1 + _num2;
        }

        private double Subtruct()
        {
            return _num1 - _num2;
        }

        private double Multiply()
        {
            return _num1 * _num2;
        }

        private double Divide()
        {
            if (_num2 == 0)
                throw new DivideByZeroException();
            else
                return _num1 / _num2;
        }

        public double GetResult()
        {
            switch (_operator)
            {
                case "+":
                    return Add();
                case "-":
                    return Subtruct();
                case "*":
                    return Multiply();
                case "/":
                    return Divide();
                default:
                    throw new Exception();
            }
        }
    }
}