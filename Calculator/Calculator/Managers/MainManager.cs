﻿using Calc.BAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calc.App.Managers
{
    class MainManager
    {
        private readonly Calculator _calculator;

        public MainManager()
        {
            _calculator = new Calculator();
        }

        private void SetCalculatorData()
        {
            _calculator.Num1 = UserInput.InputNumberPrompt("Enter first operand: ");
            _calculator.Operator = UserInput.InputOperatorPrompt("\nEnter the operator (+, -, *, /): ");
            _calculator.Num2 = UserInput.InputNumberPrompt("\nEnter second operand: ");
        }

        public void StartCalculator()
        {
            SetCalculatorData();
            try
            {
                Console.WriteLine("\n{0} {1} {2} = {3}",
                _calculator.Num1, _calculator.Operator, _calculator.Num2, _calculator.GetResult());
            }
            catch(DivideByZeroException ex)
            {
                Console.WriteLine("\nError. Can't divide by zero.");
            }            
        }
    }
}
