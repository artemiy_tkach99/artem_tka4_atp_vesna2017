﻿using System;
using Calc.BAL;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calc.App.Managers
{
    class UserInput
    {
        private static bool IsValidOperation(string input)
        {
            return Calculator.ValidOperators.Contains(input);
        }

        public static double InputNumberPrompt(string promptText)
        {
            Console.Write(promptText);
            double tempInput;
            while (!Double.TryParse(Console.ReadLine(), out tempInput))
                Console.Write("Error. Input is not a numerical value. Try again. ");
            return tempInput;
        }

        public static string InputOperatorPrompt(string promptText)
        {
            Console.Write(promptText);
            string tempInput = Console.ReadLine();
            while (!IsValidOperation(tempInput))
            {
                Console.Write("Error. Input is not a valid operation. Try again. ");
                tempInput = Console.ReadLine();
            }
            return tempInput;
        }

        public static bool RestartPrompt(string promptText)
        {
            Console.Write(promptText);
            string tempInput = Console.ReadLine();
            while (tempInput != "y" && tempInput != "n" &&
                tempInput != "Y" && tempInput != "N")
            {
                Console.Write("Error. Input is not a valid option. Try again. ");
                tempInput = Console.ReadLine();
            }

            if (tempInput == "y" || tempInput == "Y")
                return true;
            else
                return false;
        }
    }
}
