﻿using System;
using Calc.App.Managers;

namespace Calc.App
{
    class Program
    {
        static void Main(string[] args)
        {
            var manager = new MainManager();
            bool keepOn = true;
            while (keepOn)
            {
                Console.WriteLine("=== Calculator ===\n");
                manager.StartCalculator();
                keepOn = UserInput.RestartPrompt("Perform another calculation? (y/n): ");
                Console.WriteLine();
            }
        }
    }
}