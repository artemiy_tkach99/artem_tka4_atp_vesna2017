﻿using FigureArea.App.Core;
using FigureArea.BAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FigureArea.App.Managers
{
    class MainManager
    {
        private readonly Figure _figure;

        public MainManager()
        {
            _figure = new Figure();
        }

        private void SetFigureData(FigureType figure)
        {
            switch(figure)
            {
                case FigureType.Square:
                    _figure.Value1 = UserInput.InputNumberPrompt("Enter side A: ");
                    break;
                case FigureType.Rectangle:
                    _figure.Value1 = UserInput.InputNumberPrompt("Enter side A: ");
                    _figure.Value2 = UserInput.InputNumberPrompt("Enter side B: ");
                    break;
                case FigureType.Circle:
                    _figure.Value1 = UserInput.InputNumberPrompt("Enter radius: ");
                    break;
                case FigureType.Ellipse:
                    _figure.Value1 = UserInput.InputNumberPrompt("Enter major axis: ");
                    _figure.Value2 = UserInput.InputNumberPrompt("Enter minor axis: ");
                    break;
                case FigureType.Triangle:
                    _figure.Value1 = UserInput.InputNumberPrompt("Enter base length: ");
                    _figure.Value2 = UserInput.InputNumberPrompt("Enter vertical height: ");
                    break;
            }
        }

        public double CalculateArea(FigureType figure)
        {
            SetFigureData(figure);
            switch (figure)
            {
                case FigureType.Square:
                    return _figure.GetRectangleArea(_figure.Value1);
                case FigureType.Rectangle:
                    return _figure.GetRectangleArea(_figure.Value1, _figure.Value2);
                case FigureType.Circle:
                    return _figure.GetEllipseArea(_figure.Value1);
                case FigureType.Ellipse:
                    return _figure.GetEllipseArea(_figure.Value1, _figure.Value2);
                case FigureType.Triangle:
                    return _figure.GetTriangleArea(_figure.Value1, _figure.Value2);
                default:
                    throw new Exception();
            }
        }

        public void StartAreaCalculator()
        {
            Console.WriteLine("Calculate area for: ");
            Console.WriteLine("0 - Square \n1 - Rectangle \n2 - Circle \n3 - Ellipse \n4 - Triangle");
            Console.Write("\nEnter the coresponding number: ");
            FigureType figure = (FigureType)UserInput.InputInt(0, 4); 
            Console.WriteLine($"The area of your {figure} is: {CalculateArea(figure)}\n");
        }
    }


}
