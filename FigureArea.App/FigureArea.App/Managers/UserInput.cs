﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FigureArea.App.Managers
{
    class UserInput
    {
        public static double InputNumberPrompt(string promptText = "")
        {
            Console.Write(promptText);
            double tempInput;
            while (!Double.TryParse(Console.ReadLine(), out tempInput))
                Console.Write("Error. Input is not a numerical value. Try again. ");
            return tempInput;
        }

        public static int InputInt(int minValue = int.MinValue, int maxValue = int.MaxValue)
        {
            int tempInput;
            while (!int.TryParse(Console.ReadLine(), out tempInput) || (tempInput < minValue) || (tempInput > maxValue))
            {
                if(tempInput < minValue || tempInput > maxValue)
                {
                    Console.Write("Error. Number is out of bounds. Try again. ");
                    continue;
                }
                Console.Write("Error. Input is not a numerical value. Try again. ");
            }
            return tempInput;
        }

        public static bool RestartPrompt(string promptText = "")
        {
            Console.Write(promptText);
            string tempInput = Console.ReadLine();
            while (tempInput != "y" && tempInput != "n" &&
                tempInput != "Y" && tempInput != "N")
            {
                Console.Write("Error. Input is not a valid option. Try again. ");
                tempInput = Console.ReadLine();
            }

            if (tempInput == "y" || tempInput == "Y")
                return true;
            else
                return false;
        }
    }
}
