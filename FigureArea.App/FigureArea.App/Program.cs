﻿using FigureArea.App.Managers;
using System;


namespace FigureArea.App
{
    class Program
    {
        static void Main(string[] args)
        {
            var manager = new MainManager();
            bool keepOn = true;
            while (keepOn)
            {
                Console.WriteLine("=== calculator ===\n");
                manager.StartAreaCalculator();
                keepOn = UserInput.RestartPrompt("Perform another calculation? (y/n): ");
                Console.WriteLine();
            }
        }
    }
}
