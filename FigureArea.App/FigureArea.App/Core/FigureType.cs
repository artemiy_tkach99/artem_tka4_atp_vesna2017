﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FigureArea.App.Core
{
    enum FigureType
    {
        Square,
        Rectangle,
        Circle,
        Ellipse,
        Triangle
    }
}