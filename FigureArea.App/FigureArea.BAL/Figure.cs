﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FigureArea.BAL
{
    public class Figure
    {
        private double value1;
        private double value2;

        public double Value1
        {
            get { return value1; }
            set { value1 = value; }
        }

        public double Value2
        {
            get { return value2; }
            set { value2 = value; }
        }

        public double GetRectangleArea(double sideA)
        {
            return Math.Pow(sideA, 2);
        }

        public double GetRectangleArea(double sideA, double sideB)
        {
            return sideA * sideB;
        }

        public double GetEllipseArea(double radius)
        {
            return Math.PI * Math.Pow(radius, 2);
        }

        public double GetEllipseArea(double majorAxis, double minorAxis)
        {
            return Math.PI * majorAxis * minorAxis;
        }

        public double GetTriangleArea(double baseLength, double height)
        {
            return (baseLength * height) / 2;
        }
    }
}
