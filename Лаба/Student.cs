﻿namespace Лаба_1
{
    internal class Student
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string GroupName { get; set; }
        public int Age { get; set; }
    }
}