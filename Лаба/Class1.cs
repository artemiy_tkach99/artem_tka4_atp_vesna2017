﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp8
{
    public class Student
    {                
            private string _firstName;
            private string _lastName;
            private string _groupName;
            private int _age;

            public string FirstName { get { return _firstName; } set { _firstName = value; } }

            public string LastName { get { return _lastName; } set { _lastName = value; } }

            public string GroupName { get { return _groupName; } set { _groupName = value; } }

            public int Age { get { return _age; } set { _age = value; } }

            public override string ToString()
            {
                return string.Format("{0}  {1}  {2}  {3}  ", _firstName, _lastName, _age, _groupName); 
            }
    }
}
